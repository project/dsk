<?php


/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function dsk_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}
/**
 * Implements hook_install_tasks_alter().
 *
 * Here we change the function that is called for the last step of installation.
 * See below.
 *
 * @param array $tasks
 * @param array $install_state
 */
function dsk_install_tasks_alter(&$tasks, $install_state) {
  $tasks['install_finished']['function'] = 'install_dsk';
}

/**
 * Callback for the last install step.
 *
 * Above we did change the callback. Here we do first the same stuff the
 * default. As the system is now fully installed we can do anything we want.
 * Just like a "one time" module. See the comments in the source below.
 *
 * @param $install_state
 *  Current install state.
 *
 * @return
 *  String that is output to the page. Can be HTML.
 */
function install_dsk(&$install_state) {
  // Finalyse the install by calling the core function as usual. We can use
  // the HTML that is inside $base or not. Doesn't matter.
  $base = install_finished($install_state);

 /**
   * Create a dsk custom node.
   * @see http://drupal.org/node/889058
   */

  // This is a node object. It's very easy to create an array and cast it to
  // an object. See futher below for a print_r() of the full node types.
  $node = (object) array(
    // Plain string.
    'title' => 'Title',
    // Body text field. Fields are multilingual and for multiple values. So the
    // array is a bit more complictad than in D6.
    'body' => array(
      // "und" stands for "undefined" language. No german "und" :) But we
      // use the core constant here anyway.
      LANGUAGE_NONE => array(
        // First element.
        0 => array(
          // Value is the main text.
          'value' => 'Body Text',
          // The Summary if we split it up.
          'summary' => 'Summary',
          // Filtered HTML by default.
          'format' => 'filtered_html',
        )
      ),
    ),
    // Default types are "page" and "article".
    'type' => 'article',
    'language' => LANGUAGE_NONE,
    // User that creates the node.
    'uid' => 1,
  );

  // Prepare the node object to be saved. This sets some defaults and calls
  // hook_node_prepare.
  //  This also sets the nodes uid to the current user. If this is not what
  //  you want remove this function call.
  // @see http://api.drupal.org/api/drupal/modules--node--node.module/function/node_object_prepare/7
  node_object_prepare($node);


  /**
   * Add a file.
   */
  // Some filepath on our system. It's the Druplicon! :D
  $filepath = drupal_realpath('misc/druplicon.png');
  // Create managed File object and associate with Image field.
  $file = (object) array(
    'uid' => 1,
    'uri' => $filepath,
    'filemime' => file_get_mimetype($filepath),
    'status' => 1,
  );

  // We save the file to the root of the files directory.
  $file = file_copy($file, 'public://');

  $node->field_image[LANGUAGE_NONE][0] = (array)$file;

  /**
   * Add a some terms.
   */
  $node->field_tags[LANGUAGE_NONE][] = array (
    'vid' => 1,
    'tid' => 'autocreate',
    'name' => 'dsk 1',
    'vocabulary_machine_name' => 'tags'
  );

  $node->field_tags[LANGUAGE_NONE][] = array (
    'vid' => 1,
    'tid' => 'autocreate',
    'name' => 'dsk 2',
    'vocabulary_machine_name' => 'tags'
  );

  // Finally save the node object. The function uses the node object by
  // reference. So we have a nid set after this call and still can work
  // with the node.
  node_save((object) $node);

  // Create a menu entry for our new node.
  $link = array();
  $link['menu_name'] = 'main-menu';
  $link['link_title'] = 'Link Title';
  $link['link_path'] = "node/$node->nid";
  $link['options']['attributes']['title'] = 'Link description';
  menu_link_save($link);

  // Create a path alias for the node.
  $path['alias'] = 'our-great-dsk-node';
  $path['source'] = 'node/' . $node->nid;
  $path['language'] = LANGUAGE_NONE;
  path_save($path);

  // Add a comment.
  $comment = (object) array(
    'nid' => 1,
    'cid' => 0,
    'pid' => 0,
    'uid' => 1,
    'mail' => '',
    'is_anonymous' => 0,
    'homepage' => '',
    'status' => COMMENT_PUBLISHED,
    'subject' => 'dsk subject',
    'language' => LANGUAGE_NONE,
    'comment_body' => array(
      LANGUAGE_NONE => array(
        0 => array (
          'value' => 'aaa',
          'format' => 'filtered_html'
        )
      )
    ),
  );
  comment_submit($comment);
  comment_save($comment);

  /**
   * Create a custom block.
   * @see http://api.drupal.org/api/drupal/modules--block--block.admin.inc/function/block_add_block_form_submit
   */
  // Base block data for the custom block table. We need the delta.
  $delta = db_insert('block_custom')
    ->fields(array(
      'body' => 'dsk Block',
      'info' => 'dsk Block info',
      // Input format.
      'format' => 'filtered_html',
    ))
    ->execute();

  // Insert main data to the block table.
  $query = db_insert('block')->fields(array('visibility', 'pages', 'custom', 'title', 'module', 'theme', 'status', 'weight', 'delta', 'cache', 'region'));
  $query->values(array(
    'visibility' => BLOCK_VISIBILITY_NOTLISTED,
    'pages' => '',
    'custom' => BLOCK_CUSTOM_FIXED,
    'title' => 'dsk Block Title',
    'module' => 'block',
    // We only set the block for the bartik theme.
    'theme' => 'bartik',
    'status' => 1,
    'weight' => 0,
    // Delta value from above.
    'delta' => $delta,
    'cache' => DRUPAL_NO_CACHE,
    'region' => 'sidebar_first',
  ));
  $query->execute();

  // Finally clear all caches that our content can shine!
  cache_clear_all();

  // Here you can return any HTML.
  return $base . '<h1>Yay!</h1>';
}
/**
 *
 * Default profiles node object of type article:

 stdClass Object
(
    [vid] => 3
    [uid] => 1
    [title] => Article
    [log] =>
    [status] => 1
    [comment] => 2
    [promote] => 1
    [sticky] => 0
    [nid] => 3
    [type] => article
    [language] => und
    [created] => 1296594325
    [changed] => 1296594325
    [tnid] => 0
    [translate] => 0
    [revision_timestamp] => 1296594325
    [revision_uid] => 1
    [body] => Array
        (
            [und] => Array
                (
                    [0] => Array
                        (
                            [value] => Body Body Body Body Body Body Body Body Body Body Body Body
                            [summary] => Summary
                            [format] => filtered_html
                            [safe_value] =>

Body Body Body Body Body Body Body Body Body Body Body Body


                            [safe_summary] =>

Summary


                        )

                )

        )

    [field_tags] => Array
        (
            [und] => Array
                (
                    [0] => Array
                        (
                            [tid] => 1
                            [taxonomy_term] => stdClass Object
                                (
                                    [tid] => 1
                                    [vid] => 1
                                    [name] => Tag
                                    [description] =>
                                    [format] =>
                                    [weight] => 0
                                    [vocabulary_machine_name] => tags
                                    [rdf_mapping] => Array
                                        (
                                            [rdftype] => Array
                                                (
                                                    [0] => skos:Concept
                                                )

                                            [name] => Array
                                                (
                                                    [predicates] => Array
                                                        (
                                                            [0] => rdfs:label
                                                            [1] => skos:prefLabel
                                                        )

                                                )

                                            [description] => Array
                                                (
                                                    [predicates] => Array
                                                        (
                                                            [0] => skos:definition
                                                        )

                                                )

                                            [vid] => Array
                                                (
                                                    [predicates] => Array
                                                        (
                                                            [0] => skos:inScheme
                                                        )

                                                    [type] => rel
                                                )

                                            [parent] => Array
                                                (
                                                    [predicates] => Array
                                                        (
                                                            [0] => skos:broader
                                                        )

                                                    [type] => rel
                                                )

                                        )

                                    [uri] => Array
                                        (
                                            [path] => taxonomy/term/1
                                            [options] => Array
                                                (
                                                    [entity_type] => taxonomy_term
                                                    [entity] => stdClass Object
 *RECURSION*
                                                )

                                        )

                                )

                        )

                )

        )

    [field_image] => Array
        (
            [und] => Array
                (
                    [0] => Array
                        (
                            [fid] => 1
                            [alt] =>
                            [title] =>
                            [uid] => 1
                            [filename] => 01.jpg
                            [uri] => public://field/image/01.jpg
                            [filemime] => image/jpeg
                            [filesize] => 871769
                            [status] => 1
                            [timestamp] => 1296594325
                            [rdf_mapping] => Array
                                (
                                )

                        )

                )

        )

    [rdf_mapping] => Array
        (
            [field_image] => Array
                (
                    [predicates] => Array
                        (
                            [0] => og:image
                            [1] => rdfs:seeAlso
                        )

                    [type] => rel
                )

            [field_tags] => Array
                (
                    [predicates] => Array
                        (
                            [0] => dc:subject
                        )

                    [type] => rel
                )

            [rdftype] => Array
                (
                    [0] => sioc:Item
                    [1] => foaf:Document
                )

            [title] => Array
                (
                    [predicates] => Array
                        (
                            [0] => dc:title
                        )

                )

            [created] => Array
                (
                    [predicates] => Array
                        (
                            [0] => dc:date
                            [1] => dc:created
                        )

                    [datatype] => xsd:dateTime
                    [callback] => date_iso8601
                )

            [changed] => Array
                (
                    [predicates] => Array
                        (
                            [0] => dc:modified
                        )

                    [datatype] => xsd:dateTime
                    [callback] => date_iso8601
                )

            [body] => Array
                (
                    [predicates] => Array
                        (
                            [0] => content:encoded
                        )

                )

            [uid] => Array
                (
                    [predicates] => Array
                        (
                            [0] => sioc:has_creator
                        )

                    [type] => rel
                )

            [name] => Array
                (
                    [predicates] => Array
                        (
                            [0] => foaf:name
                        )

                )

            [comment_count] => Array
                (
                    [predicates] => Array
                        (
                            [0] => sioc:num_replies
                        )

                    [datatype] => xsd:integer
                )

            [last_activity] => Array
                (
                    [predicates] => Array
                        (
                            [0] => sioc:last_activity_date
                        )

                    [datatype] => xsd:dateTime
                    [callback] => date_iso8601
                )

        )

    [cid] => 0
    [last_comment_timestamp] => 1296594325
    [last_comment_name] =>
    [last_comment_uid] => 1
    [comment_count] => 0
    [name] => comm-press
    [picture] => 0
    [data] =>
    [uri] => Array
        (
            [path] => node/3
            [options] => Array
                (
                    [entity_type] => node
                    [entity] => stdClass Object
 *RECURSION*
                )

        )

    [entity_view_prepared] => 1
)

 */