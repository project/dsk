
Just install Drupal the usual way, but use DSK instead of standard or minimal during the first step.

For right now this is the same as the default profile, but it creates a node with some tags, a comment, a menu item and an image as well as a block in a sidebar. We will have to fill this with meaningful content and some elements that need styling. Please provide feedback in the issue queue.